export default function reducer(
  state = { user: [], fetching: false, fetched: false, creating: false, created: false, error: null, updatedUser: false, errorUpdateUser: false },
  action
) {
  switch (action.type) {
    case "FETCH_USER": {
      return { ...state, fetching: true };
    }
    case "FETCH_USER_REJECTED": {
      return { ...state, fetching: false, error: action.payload };
    }
    case "FETCH_USER_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        user: action.payload,
      };
    }

    case "CREATE_USER": {
      return { ...state, creating: true };
    }
    case "CREATE_USER_FULFILLED": {
      return {
        ...state,
        user: [...state.user, action.payload],
        creating: false,
        error: false,
        created: true,
      };
    }

    case "CREATE_USER_REJECTED": {
      return {
        ...state,
        creating: false,
        error: action.payload,
      };
    }

    case "DELETE_USER_FULFILLED": {
      //delete user
      return {
        user: state.user.filter(user => user.id !== action.payload),
        updatedUser: true
      };
    }
    case "UPDATE_USER_REJECTED": {
      return { ...state, fetching: false, errorUpdateUser: true };
    }

    case "UPDATE_USER": {
      return { ...state, updatedUser: false, errorUpdateUser: false };
    }

    default: {
      return state;
    }
  }
}
