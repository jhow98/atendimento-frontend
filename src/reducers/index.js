import { combineReducers } from "redux";

import user from "./userReducer";
import procedure from "./procedureReducer";
import consultation from "./consultationReducer";
import report from "./reportReducer";

export default combineReducers({
  user,
  procedure,
  consultation,
  report,
});
