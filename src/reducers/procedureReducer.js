export default function reducer(
  state = { procedure: [], fetching: false, fetched: false, creating: false, created: false, error: false, updatedProcedure: false },
  action
) {
  switch (action.type) {
    case "FETCH_PROCEDURE": {
      return { ...state, fetching: true };
    }
    case "FETCH_PROCEDURE_REJECTED": {
      return { ...state, fetching: false, error: action.payload };
    }
    case "FETCH_PROCEDURE_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        procedure: action.payload,
      };
    }

    case "CREATE_PROCEDURE": {
      return { ...state, creating: true, created: false, updatedProcedure:false };
    }
    case "CREATE_PROCEDURE_FULFILLED": {
      return {
        ...state,
        procedure: [...state.procedure, action.payload],
        creating: false,
        error: false,
        created: true,
      };
    }

    case "CREATE_PROCEDURE_REJECTED": {
      return {
        ...state,
        creating: false,
        error: action.payload,
      };
    }

    case "DELETE_PROCEDURE_FULFILLED": {
      //delete procedure
      return {
       procedure: state.procedure.filter(procedure => procedure.id !== action.payload),
       updatedProcedure: true
      };
    }
    case "UPDATE_PROCEDURE_REJECTED": {
      return { ...state, error: true};
    }

    case "UPDATE_PROCEDURE": {
      return { ...state, updatedProcedure: false, error: false};
    }

    default: {
      return state;
    }
  }
}
