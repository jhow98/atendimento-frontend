export default function reducer(
  state = {
    consultation: [],
    fetching: false,
    fetched: false,
    creating: false,
    created: false,
    error: null,
  },
  action
) {
  switch (action.type) {
    case "FETCH_CONSULTATION": {
      return { ...state, fetching: true };
    }
    case "FETCH_CONSULTATION_REJECTED": {
      return { ...state, fetching: false, error: action.payload };
    }
    case "FETCH_CONSULTATION_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        consultation: action.payload,
      };
    }

    
    case "CREATE_CONSULTATION": {
      return { ...state, creating: true };
    }
    case "CREATE_CONSULTATION_FULFILLED": {
      return {
        ...state,
        consultation: [...state.consultation, action.payload],
        creating: false,
        error: false,
        created: true,
      };
    }

    case "CREATE_CONSULTATION_REJECTED": {
      return {
        ...state,
        creating: false,
        error: action.payload,
      };
    }

    case "DELETE_CONSULTATION_FULFILLED": {
      //delete consultation
      return {
        consultation: state.consultation.filter(
          (consultation) => consultation.id !== action.payload
        ),
      };
    }
    case "UPDATE_CONSULTATION_REJECTED": {
      return { ...state, fetching: false };
    }    

    default: {
      return state;
    }
  }
}
