export default function reducer(
  state = {
    report: [],
    fetching: false,
    fetched: false,
    creating: false,
    error: null,
  },
  action
) {
  switch (action.type) {
    case "FETCH_REPORT_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        report: [...state.report, action.payload],
      };
    }

    case "FETCH_REPORT_REJECTED": {
      return {
        ...state,
        fetching: false,
        error: true,
      };
    }


    default: {
      return state;
    }
  }
}
