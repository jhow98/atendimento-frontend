/* eslint-disable no-template-curly-in-string */
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { createUser } from "./../../actions/userActions";
import { Form, Input, Button, message, DatePicker } from "antd";
import { CSVReader } from "react-papaparse";
// import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";

const validateMessages = {
  required: "${label} é obrigatório",
  types: {
    email: "${label} não é válito!",
    number: "${label} is not a validate number!",
    integer: "${label} is not a validate integer!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const UserRegisterForm = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const [uploaded, setUploaded] = useState();

  const handleSetProfilePhoto = (file) => {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("Você só pode enviar arquivos do tipo JPG/PNG!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("A imagem deve ser de no máximo 2MB!");
    }
    if (isJpgOrPng && isLt2M) {
      setUploaded(file);
    }
  };
  const submitCSV = (csvInfo) => {
    csvInfo.pop(); //remove o último ítem (limitadores)
    csvInfo.shift(); //remove o primeiro item (header)

    csvInfo.map((user) => {
      const values = {
        name: user.data[0],
        email: user.data[1],
        birthday: user.data[2],
        phone: parseInt(user.data[3]),
      };
      return dispatch(createUser(values));
    });
  };

  const onFinish = ({ name, email, birthday, phone }) => {
    const values = new FormData();
    const birthdayFormatted = birthday.format("YYYY-MM-DD");
    values.append("name", name);
    values.append("phone", phone);
    values.append("birthday", birthdayFormatted);
    values.append("email", email);
    values.append("userphoto", uploaded);

    dispatch(createUser(values));
  };
  const [form] = Form.useForm();

  useEffect(() => {
    if (user.created) {
      message.success("Usuário cadastrado com sucesso!");
      //@todo: limpar formulário
    }
  }, [form, user.created]);

  useEffect(() => {
    if (user.error) {
      message.error(user.error);
    }
  }, [user.error]);

  const layout = {
    labelCol: {
      span: 5,
    },
    wrapperCol: {
      span: 12,
    },
  };

  const handleOnDrop = (data) => {
    submitCSV(data);
  };

  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        padding: "20px",
      }}
    >
      <Form
        {...layout}
        name="nest-messages"
        onFinish={onFinish}
        validateMessages={validateMessages}
        style={{ width: "45%", justifyContent: "center", alignItems: "center" }}
      >
        <h1>Cadastro de Pacientes</h1>

        <Form.Item
          name={["name"]}
          label="Nome"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="birthday"
          label="Nascimento"
          rules={[
            {
              type: "object",
              required: true,
            },
          ]}
        >
          <DatePicker placeholder="Selecione a data" />
        </Form.Item>

        <Form.Item
          name={["phone"]}
          label="Telefone"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name={["email"]}
          label="E-mail"
          rules={[
            {
              type: "email",
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <label htmlFor="avatar">Escolha uma foto de perfil:</label>
        <input
          id="avatar"
          className="avatar-uploader"
          type="file"
          onChange={(event) => setUploaded(event.target.files[0])}
        />

        <Form.Item
          style={{ marginTop: "20px" }}
          wrapperCol={{ ...layout.wrapperCol, offset: 10 }}
        >
          <Button type="primary" htmlType="submit" loading={user.creating}>
            Cadastrar Usuário
          </Button>
        </Form.Item>
      </Form>
      <p style={{ fontSize: 19, color: "grey" }}>OU</p>
      <div style={{ width: "45%" }}>
        <CSVReader
          onDrop={(e) => handleOnDrop(e)}
          onError={(e) => message.error(e)}
          addRemoveButton
          removeButtonColor="#659cef"
        >
          <span>
            Arraste ou selecione um arquivo CSV com dados de pacientes para
            cadastrar
          </span>
        </CSVReader>
      </div>
    </div>
  );
};

export default UserRegisterForm;
