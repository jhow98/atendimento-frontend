export {default as ConsultationForm} from './ConsultationForm';
export {default as ProcedureListTable} from './ProcedureListTable';
export {default as ProcedureRegisterForm} from './ProcedureRegisterForm';
export {default as UserListTable} from './UserListTable';
export {default as UserRegisterForm} from './UserRegisterForm';
