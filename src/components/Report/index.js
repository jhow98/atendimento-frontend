import React from "react";
import "antd/dist/antd.css";
import { Drawer, Divider, Col, Row } from "antd";
import { Table, Space, Avatar } from "antd";
import { UserOutlined } from "@ant-design/icons";
import getDurationByMililisecondsInterval from "./../../utils/getDurationByMililisecondsInterval";
import moment from "moment";

export default function Report({ report }) {
  const DescriptionItem = ({ title, content }) => (
    <div className="site-description-item-profile-wrapper">
      <p className="site-description-item-profile-p-label">{title}:</p>
      {content}
    </div>
  );

  const user = report[0].user[0];
  const procedures = report[0].procedures;
  const firstName = user.user_name.split(" ")[0];
  const complaints = report[0].consultation_complaint.split(",");

  const columnsProcedures = [
    {
      title: "Nome",
      dataIndex: "procedure_name",
      key: "procedure_name",
    },
    {
      title: "Duração Média",
      dataIndex: "procedure_average_duration",
      key: "procedure_average_duration",
      render: (text) => (
        <Space size="small">{parseInt(text) / 60000}minutos</Space>
      ),
    },
    {
      title: "Preço",
      dataIndex: "procedure_price",
      key: "procedure_price",
      render: (text) => (
        <Space size="small">
          R${text}
        </Space>
      ),
    },
  ];

  const getUserAvatar = () => {
    if (user.user_photo) {
      return (
        <img
          src={`https://atendimento-backend.herokuapp.com/files/${user.user_photo}`}
          alt={`Foto de perfil de ${firstName}`}
        />
      );
    } else {
      return <Avatar size={64} icon={<UserOutlined />} />;
    }
  };

  return (
    <Drawer width={640} placement="right" closable={false} visible={true}>
      <p
        className="site-description-item-profile-p"
        style={{ marginBottom: 24 }}
      >
        <Avatar
          style={{ marginRight: "15px" }}
          size={64}
          icon={getUserAvatar()}
        />
        <b>Relatório da consulta de {firstName}</b>
      </p>
      <p className="site-description-item-profile-p">
        <b>Dados pessoais</b>
      </p>
      <Row>
        <Col span={12}>
          <DescriptionItem title="Nome Completo" content={user.user_name} />
        </Col>
        <Col span={12}>
          <DescriptionItem title="E-mail" content={user.user_email} />
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <DescriptionItem title="Telefone" content={user.user_phone} />
        </Col>
        <Col span={12}>
          <DescriptionItem
            title="Data de nascimento"
            content={moment(user.user_birthday).format("DD/MM/YYYY")}
          />
        </Col>
      </Row>

      <Divider />

      <Row>
        <p className="site-description-item-profile-p">
          <b>Procedimentos Recomendados</b>
        </p>
        <Col span={24}>
          <Table
            pagination={false}
            columns={columnsProcedures}
            dataSource={procedures}
          />
        </Col>
      </Row>

      <Divider />

      <p className="site-description-item-profile-p">
        <b>Queixas do paciente</b>
      </p>
      <Row>
        <Col span={12}>
          <ul>
            {complaints.map((complaint) => {
              return <li>{complaint.replace(/[\\}{"]/g, "")}</li>;
            })}
          </ul>
        </Col>
      </Row>

      <Divider />

      <p className="site-description-item-profile-p">
        <b>Informações adicionais</b>
      </p>

      <Row>
        <h5>{`Valor total dos procedimentos: ${report[0].totalPrice.toLocaleString(
          "pt-br",
          { style: "currency", currency: "BRL" }
        )}`}</h5>
      </Row>

      <Row>
        <h5>{`Duração desta consulta: ${(report[0].consultation_duration / 60000).toFixed(2)} minutos`}</h5>
      </Row>

      <Row>
        <h5>{`Tempo total necessário para todos os procedimentos: ${(report[0].totalTime / 60000).toFixed(2)} minutos`}</h5>
      </Row>
    </Drawer>
  );
}
