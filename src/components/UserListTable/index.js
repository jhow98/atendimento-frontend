/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser, fetchUser } from "./../../actions/userActions";
import { Table, Space, message, Popconfirm } from "antd";
import UserDetailsModal from "./../UserDetailsModal";
import moment from "moment";

const UserListTable = (props) => {
  const [userShow, setUserShow] = useState(null);
  const dispatch = useDispatch();
  const users = useSelector((state) => state.user);

  useEffect(() => {
    dispatch(fetchUser());
  }, [dispatch]);

  useEffect(() => {
    if (users.errorUpdateUser) {
      message.error(
        "Não foi possível executar esta ação com o paciente selecionado."
      );
    }

    if (users.updatedUser) {
      message.success("Paciente excluído.");
    }
  }, [users]);

  const handleDeleteUser = (id) => {
    dispatch(deleteUser(id));
  };

  const handleShowUserDetails = (user) => {
    setUserShow(user);
  };

  const columns = [
    {
      title: "Nome",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Space size="middle">
          <a
            onClick={() => {
              handleShowUserDetails(record);
            }}
          >
            {record.name}
          </a>
        </Space>
      ),
    },
    {
      title: "Nascimento",
      dataIndex: "birthday",
      key: "birthday",
      render: (text) => moment(text).format("DD/MM/YYYY"),
    },
    {
      title: "E-mail",
      dataIndex: "email",
      key: "email",
    },

    {
      title: "Telefone",
      dataIndex: "phone",
      key: "phone",
    },

    {
      title: "Ações",
      key: "delete",
      render: (text, record) => (
        <Space size="middle">
          <Popconfirm
            title={`Confirma a exclusão de ${record.name}?`}
            okText="Sim"
            cancelText="Não"
            onConfirm={() => {
              handleDeleteUser(record.id);
            }}
          >
            <a>Excluir Paciente</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  const data = users.user;
  return (
    <>
      <Table columns={columns} dataSource={data} />
      {userShow && <UserDetailsModal user={userShow} />}
    </>
  );
};

export default UserListTable;
