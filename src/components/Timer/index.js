import React from "react";
import { useStopwatch } from "react-timer-hook";
import { Statistic } from "antd";
import { ClockCircleOutlined } from "@ant-design/icons";

export default function Timer() {
  let { seconds, minutes, hours } = useStopwatch({ autoStart: true });

  //as condicionais abaixo mantém o formato HH:MM:SS na exibição do timer.
  if (seconds < 10) {
    seconds = "0".concat(seconds);
  }
  if (minutes < 10) {
    if (minutes < 1) {
      minutes = "00";
    } else {
      seconds = "0".concat(seconds);
    }
  }
  if (hours < 10) {
    if (hours < 1) {
      hours = "00";
    } else {
      seconds = "0".concat(seconds);
    }
  }
  return (
    <Statistic
      style={{ float: "right" }}
      title="Tempo de atendimento"
      value={`${hours}:${minutes}:${seconds}`}
      prefix={<ClockCircleOutlined />}
    />
  );
}
