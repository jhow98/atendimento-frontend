import React, { useState, useEffect } from "react";
import { Modal, Button, Avatar } from "antd";
import { UserOutlined } from "@ant-design/icons";
import moment from "moment";

const UserDetailsModal = (props) => {
  const [visible, setVisible] = useState(true);
  const { user } = props;

  useEffect(() => {
    setVisible(true);
  }, [user]);

  const handleClose = () => {
    setVisible(false);
  };

  const getUserAvatar = (photo) => {
    if (photo) {
      return (
        <img
          src={`https://atendimento-backend.herokuapp.com/files/${photo}`}
          alt={`Foto de perfil`}
        />
      );
    } else {
      return <Avatar size={64} icon={<UserOutlined />} />;
    }
  };

  return (
    <Modal
      visible={visible}
      title="Detalhes do usuário"
      footer={[
        <Button key="submit" type="primary" onClick={handleClose}>
          Fechar
        </Button>,
      ]}
    >
      <h2>Sobre {user.name}</h2>

      <Avatar style={{marginBottom: '15px'}} shape="square" size={64} icon={getUserAvatar(user.photo)} />

      <p>
        <b>E-mail:</b> {user.email}
      </p>
      <p>
        <b>Telefone:</b> {user.phone}
      </p>
      <p>
        <b>Nascimento:</b> {moment(user.birthday).format("DD/MM/YYYY")}
      </p>
    </Modal>
  );
};

export default UserDetailsModal;
