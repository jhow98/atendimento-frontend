/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteProcedure, fetchProcedure } from "./../../actions/procedureActions";
import { Table, Space, Popconfirm } from "antd";

const ProcedureListTable = () => {
  const dispatch = useDispatch();
  const procedures = useSelector((state) => state.procedure);
  useEffect(() => {
    dispatch(fetchProcedure());
  }, [dispatch]);

  const handledeleteProcedure = (id) => {
    dispatch(deleteProcedure(id));
  };

  const columns = [
    {
      title: "Nome",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Duração Média",
      dataIndex: "average_duration",
      key: "average_duration",
      render: (text) => <Space size="small">{parseInt(text)/60000}minutos</Space>,
    },

    {
      title: "preço",
      dataIndex: "price",
      key: "price",
      render: (text) => <Space size="small">R${text}</Space>
    },
    {
      title: "Ações",
      key: "delete",
      render: (text, record) => (
        <Space size="middle">
          <Popconfirm
            title={`Confirma a exclusão de ${record.name}?`}
            okText="Sim"
            cancelText="Não"
            onConfirm={() => {
              handledeleteProcedure(record.id);
            }}
          >
            <a>Excluir Procedimento</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  const data = procedures.procedure;

  return <Table columns={columns} dataSource={data} />;
};

export default ProcedureListTable;
