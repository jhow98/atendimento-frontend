import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { fetchProcedure } from "./../../actions/procedureActions";
import { fetchUser } from "./../../actions/userActions";
import { createConsultation } from "./../../actions/consultationActions";
import { fetchReport } from "./../../actions/reportActions";
import Timer from "./../Timer";
import Report from "./../Report";

import getConsultationDuration from "./../../utils/getConsultationDuration";

import {
  Steps,
  Button,
  message,
  Checkbox,
  Select,
  Row,
  Col,
  Card,
  notification,
} from "antd";
import { SmileOutlined } from "@ant-design/icons";

const { Step } = Steps;

const ConsultationForm = () => {
  const [current, setCurrent] = useState(0); //Step atual
  const [proceduresSelected, setProceduresSelected] = useState([]); //Lista de procedimentos selecionados
  const [selectedUser, setSelectedUser] = useState(); //Usuário selecionado para a consulta
  const [complaints, setComplaints] = useState([]); //Usuário selecionado para a consulta
  const [startTime, setStartTime] = useState([]); //Horário de início da consulta
  const consultation = useSelector((state) => state.consultation);
  const report = useSelector((state) => state.report);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProcedure());
    dispatch(fetchUser());
  }, [dispatch]);

  useEffect(() => {
    if (consultation.created) {
      notification.open({
        message: "Consulta finalizada.",
        description:
          "Estamos gerando um relatório completo com os dados detalhados da consulta e projeção de tempo do tratamento",
        icon: <SmileOutlined style={{ color: "#108ee9" }} />,
      });
      setSelectedUser("");
      setComplaints("");
      setCurrent(0);
      setProceduresSelected([]);
      dispatch(fetchReport(consultation.consultation.map((item) => item.id)));
    }

    if (consultation.error) {
      message.error("Não foi possível registrar a consulta.");
    }
  }, [consultation, dispatch]);

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const First = () => {
    const usersList = useSelector((state) => state.user);
    const { Option } = Select;

    const complaintsList = [
      "Dor de cabeça",
      "Tontura, perda de sentido",
      "Perda do olfato/paladar",
      "Mal estar",
      "Falta de Apetite",
    ];

    function handleChangePacient(value) {
      message.success("Consulta iniciada.");
      setSelectedUser(value);
      setStartTime(new Date());
    }

    function handleChangeComplaint(checked, complaintChanged) {
      //@TODO: neste caso também não funcionou salvar no setState, investigar.
      let NewComplaint = complaints;
      //Solução temporária.
      if (checked) {
        NewComplaint.push(complaintChanged);
        setComplaints(NewComplaint);
      } else {
        NewComplaint.pop(complaintChanged);
        setComplaints(NewComplaint);
      }
    }
    return (
      <>
        {report.fetched && <Report report={report.report} />}
        <h1>Informações iniciais</h1>
        <h4>Identificação do paciente</h4>

        <Select
          showSearch
          style={{ width: 300, marginBottom: "20px" }}
          placeholder="Selecione o Paciente"
          optionFilterProp="children"
          onChange={handleChangePacient}
          loading={usersList.user.fetching}
          value={selectedUser}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {usersList.user.map((user) => {
            return <Option value={user.id}>{user.name}</Option>;
          })}
        </Select>

        <h4>Quais são as queixas?</h4>

        {complaintsList.map((complaint) => {
          return (
            <Checkbox
              style={{ marginBottom: "150px" }}
              onChange={(e) => {
                handleChangeComplaint(e.target.checked, complaint);
              }}
            >
              {complaint}
            </Checkbox>
          );
        })}
      </>
    );
  };

  const Second = () => {
    const proceduresList = useSelector((state) => state.procedure);

    const handleSelectProcedure = (checked, id) => {
      //@TODO: neste caso também não funcionou salvar no setState, investigar.
      let procedures = proceduresSelected;
      //Solução temporária.

      if (checked) {
        procedures.push(id);
        setProceduresSelected(procedures);
      } else {
        procedures.pop(id);
        setProceduresSelected(procedures);
      }
    };

    return (
      <>
        <h1>Quais os procedimentos recomendados?</h1>
        {proceduresList.procedure.map((procedure) => {
          return (
            <Checkbox
              style={{ marginBottom: "150px" }}
              onChange={(e) => {
                handleSelectProcedure(e.target.checked, procedure.id);
              }}
            >
              {procedure.name}
            </Checkbox>
          );
        })}
      </>
    );
  };

  const Third = () => {
    const usersList = useSelector((state) => state.user);
    const user = usersList.user.filter((user) => user.id === selectedUser);

    return (
      <>
        <h3 style={{ marginTop: "20px" }}>Resumo do atendimento</h3>
        <div className="site-card-wrapper" style={{ marginBottom: "130px" }}>
          <Row gutter={16}>
            <Col span={8}>
              <Card title="Nome do Paciente" bordered={false}>
                {user[0].name}
              </Card>
            </Col>
            <Col span={8}>
              <Card title="Quantidade de procedimentos" bordered={false}>
                {proceduresSelected.length}
              </Card>
            </Col>
            <Col span={8}>
              <Card title="Queixas do paciente" bordered={false}>
                {complaints}
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  };
  const steps = [
    {
      title: "Triagem",
      content: <First />,
    },
    {
      title: "Procedimentos",
      content: <Second />,
    },
    {
      title: "Resumo",
      content: <Third />,
    },
  ];

  const handleFinish = () => {
    const consultationData = {
      user_id: selectedUser,
      duration: getConsultationDuration(startTime),
      complaint: complaints,
      procedures: proceduresSelected,
    };
    dispatch(createConsultation(consultationData));
  };
  return (
    <div>
      <Steps current={current}>
        {steps.map((item) => (
          <Step key={item.title} title={item.title} />
        ))}
      </Steps>
      <div className="steps-content">{steps[current].content}</div>
      <div className="steps-action" style={{ paddingBottom: "25px" }}>
        {current < steps.length - 1 && (
          <Button
            type="primary"
            onClick={() => next()}
            disabled={!selectedUser}
          >
            Próximo
          </Button>
        )}
        {current === steps.length - 1 && (
          <Button
            type="primary"
            loading={consultation.creating}
            onClick={() => {
              handleFinish();
            }}
          >
            Finalizar Consulta
          </Button>
        )}
        {current > 0 && (
          <Button style={{ margin: "0 8px" }} onClick={() => prev()}>
            Anterior
          </Button>
        )}
        {selectedUser && <Timer />}
      </div>
    </div>
  );
};

export default ConsultationForm;
