/* eslint-disable no-template-curly-in-string */
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { createProcedure } from "./../../actions/procedureActions";
import { Form, Input, Button, message } from "antd";

const validateMessages = {
  required: "${label} é obrigatório",
};

const ProcedureRegisterForm = () => {
  const dispatch = useDispatch();

  const onFinish = ({name, average_duration, price}) => {
    dispatch(createProcedure({
      name: name,
      average_duration: average_duration * 60000, //Necessário para salvar a duração em milisegundos na base.
      price: price
    }));
  };
  const procedure = useSelector((state) => state.procedure);

  useEffect(() => {
    if (procedure.created) {
      message.success("Procedimento criado!");
      //@todo: limpar formulario
    }

    if (procedure.error) {
      message.error(
        "Não foi possível executar esta ação."
      );
    }

    if (procedure.updatedProcedure) {
      message.success("Procedimento excluído.");
    }
  }, [procedure]);

  const layout = {
    labelCol: {
      span: 2,
    },
    wrapperCol: {
      span: 8,
    },
  };

  return (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={onFinish}
      validateMessages={validateMessages}
    >
      <h1>Cadastro de Procedimentos</h1>

      <Form.Item
        name={["name"]}
        label="Nome"
        
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input placeholder="Nome do procedimento" />
      </Form.Item>

      <Form.Item
        name={["average_duration"]}
        
        label="Duração em min."
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input placeholder="Em minutos" />
      </Form.Item>

      <Form.Item
        name={["price"]}
        label="Preço"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input placeholder="Valor do procedimento" />
      </Form.Item>

      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 7 }}>
        <Button type="primary" htmlType="submit" loading={procedure.creating}>
          Cadastrar Procedimento
        </Button>
      </Form.Item>
    </Form>
  );
};

export default ProcedureRegisterForm;
