import moment from "moment";

const getDurationByMililisecondsInterval = (diff) => {
  const d = moment.duration(diff, "milliseconds");
  let hours = Math.floor(d.asHours());
  let mins = Math.floor(d.asMinutes()) - hours * 60;
  let secs = Math.floor(d.asSeconds()) - mins * 60;
  return hours + "horas, " + mins + "minutos e :" + secs + "segundos";
};

export default getDurationByMililisecondsInterval;
