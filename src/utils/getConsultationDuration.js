const getConsultationDuration = (startTime) => {
  const now = new Date();
  var diff = now - startTime; //milliseconds interval
  return diff;
};

export default getConsultationDuration;
