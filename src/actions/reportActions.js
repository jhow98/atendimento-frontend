import axios from "axios";

export function fetchReport(payload) {
  return function (dispatch) {
    axios
      .get("https://atendimento-backend.herokuapp.com/consultations/" + payload + "/report")
      .then((response) => {
        dispatch({
          type: "FETCH_REPORT_FULFILLED",
          payload: response.data,
        });
      })
      .catch((err) => {
        dispatch({ type: "FETCH_REPORT_REJECTED", payload: err });
      });
  };
}