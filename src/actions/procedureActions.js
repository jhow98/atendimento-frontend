import axios from "axios";

export function fetchProcedure() {
  return function (dispatch) {
    axios
      .get("https://atendimento-backend.herokuapp.com/procedures")
      .then((response) => {
        dispatch({ type: "FETCH_PROCEDURE_FULFILLED", payload: response.data });
      })
      .catch((err) => {
        dispatch({ type: "FETCH_PROCEDURE_REJECTED", payload: err });
      });
  };
}

export function createProcedure(payload) {
  return function (dispatch) {
    dispatch({ type: "CREATE_PROCEDURE" });
    axios
      .post("https://atendimento-backend.herokuapp.com/procedures", payload)
      .then((response) => {
        payload = response.data[0];
        dispatch({ type: "CREATE_PROCEDURE_FULFILLED", payload: payload });
      })
      .catch((err) => {
        if (err.message === "Network Error") {
          dispatch({
            type: "CREATE_PROCEDURE_REJECTED",
            payload: "Houve um erro inesperado. Tente novamente mais tarde!",
          });
        } else {
          dispatch({
            type: "CREATE_PROCEDURE_REJECTED",
            payload: err.message,
          });
        }
      });
  };
}

export function deleteProcedure(payload) {
  return function (dispatch) {
    dispatch({ type: "UPDATE_PROCEDURE"});
    axios
      .delete("https://atendimento-backend.herokuapp.com/procedures/" + payload)
      .then((response) => {
        dispatch({ type: "DELETE_PROCEDURE_FULFILLED", payload: payload });
        
      })
      .catch((err) => {
        dispatch({ type: "UPDATE_PROCEDURE_REJECTED", payload: err });
      });
  };
}
