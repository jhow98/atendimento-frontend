import axios from "axios";

export function fetchUser() {
  return function (dispatch) {
    axios
      .get("https://atendimento-backend.herokuapp.com/users")
      .then((response) => {
        dispatch({ type: "FETCH_USER_FULFILLED", payload: response.data });
      })
      .catch((err) => {
        dispatch({ type: "FETCH_USER_REJECTED", payload: err });
      });
  };
}

export function createUser(payload) {
  return function (dispatch) {
    dispatch({ type: "CREATE_USER" });
    axios
      .post("https://atendimento-backend.herokuapp.com/users", payload)
      .then((response) => {
        payload = response.data[0];
        dispatch({ type: "CREATE_USER_FULFILLED", payload: payload });
      })
      .catch((err) => {
        if (err.message === "Network Error") {
          dispatch({
            type: "CREATE_USER_REJECTED",
            payload: "Houve um erro inesperado. Tente novamente mais tarde!",
          });
        } else {
          dispatch({
            type: "CREATE_USER_REJECTED",
            payload: err.message,
          });
        }
      });
  };
}

export function deleteUser(payload) {
  return function (dispatch) {
    dispatch({ type: "UPDATE_USER"});
    axios
      .delete("https://atendimento-backend.herokuapp.com/users/" + payload)
      .then((response) => {
        dispatch({ type: "DELETE_USER_FULFILLED", payload: payload });
        
      })
      .catch((err) => {
        dispatch({ type: "UPDATE_USER_REJECTED", payload: err });
      });
  };
}
