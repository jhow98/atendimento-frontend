import axios from "axios";

export function fetchConsultation() {
  return function (dispatch) {
    axios
      .get("https://atendimento-backend.herokuapp.com/consultations")
      .then((response) => {
        dispatch({
          type: "FETCH_CONSULTATION_FULFILLED",
          payload: response.data,
        });
      })
      .catch((err) => {
        dispatch({ type: "FETCH_CONSULTATION_REJECTED", payload: err });
      });
  };
}

export function createConsultation(payload) {
  return function (dispatch) {
    dispatch({ type: "CREATE_CONSULTATION" });
    axios
      .post("https://atendimento-backend.herokuapp.com/consultations", payload)
      .then((response) => {
        payload = response.data;
        dispatch({ type: "CREATE_CONSULTATION_FULFILLED", payload: payload });
      })
      .catch((err) => {
        if (err.message === "Network Error") {
          dispatch({
            type: "CREATE_CONSULTATION_REJECTED",
            payload: "Houve um erro inesperado. Tente novamente mais tarde!",
          });
        } else {
          dispatch({
            type: "CREATE_CONSULTATION_REJECTED",
            payload: err.message,
          });
        }
      });
  };
}

//Action criada para uso em futuras implementações
export function deleteConsultation(payload) {
  return function (dispatch) {
    axios
      .delete(
        "https://atendimento-backend.herokuapp.com/consultations/" + payload
      )
      .then((response) => {
        dispatch({ type: "DELETE_CONSULTATION_FULFILLED", payload: payload });
      })
      .catch((err) => {
        dispatch({ type: "UPDATE_CONSULTATION_REJECTED", payload: err });
      });
  };
}
