import React, { useState } from "react";

import { Layout, Menu, Breadcrumb } from "antd";
import * as Components from "./../../components";

const { Header, Content, Footer } = Layout;
export default function Home() {
  const [activeSection, setActiveSection] = useState("Pacientes");

  return (
    <Layout className="layout">
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item
            key="1"
            onClick={() => {
              setActiveSection("Pacientes");
            }}
          >
            Pacientes
          </Menu.Item>
          <Menu.Item
            key="2"
            onClick={() => {
              setActiveSection("Procedimentos");
            }}
          >
            Procedimentos
          </Menu.Item>
          <Menu.Item
            key="3"
            onClick={() => {
              setActiveSection("Consulta");
            }}
          >
            Consulta
          </Menu.Item>
        </Menu>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>Início</Breadcrumb.Item>
          <Breadcrumb.Item>{activeSection}</Breadcrumb.Item>
        </Breadcrumb>
        {activeSection === "Pacientes" && (
          <div className="site-layout-content">
            <Components.UserRegisterForm />
            <Components.UserListTable />
          </div>
        )}

        {activeSection === "Procedimentos" && (
          <div className="site-layout-content">
            <Components.ProcedureRegisterForm />
            <Components.ProcedureListTable />
          </div>
        )}

        {activeSection === "Consulta" && (
          <div className="site-layout-content">
            <Components.ConsultationForm />
          </div>
        )}
      </Content>
      <Footer style={{ textAlign: "center" }}>Atendimento</Footer>
    </Layout>
  );
}
