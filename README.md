## Sobre o projeto

O projeto Atendimento - Backend utiliza, dentre outros, os seguintes recursos:

- [React Router](https://www.npmjs.com/package/react-router)
- [Syled Components](https://styled-components.com/)
- [@testing-library/react](https://testing-library.com/docs/react-testing-library/intro)
- [Prop Types](https://www.npmjs.com/package/prop-types)
- [Redux](https://redux.js.org/)
- [Ant Design](https://ant.design/)  


## Instalação em ambiente de desenvolvimento

Executar os seguintes comandos:

1. yarn
2. yarn dev
## License

O Node tem sua licença open-source [MIT license](https://opensource.org/licenses/MIT).

## CI/CD

A rotina de CI/CD irá ser executada da seguinte maneira:

1. Commit no branch develop: Build, Testes e Deploy em ambiente de testes (staging). URL: https://atendimento-frontend-staging.herokuapp.com/
2. Commit no master : Build, Testes e Deploy em ambiente de produção. URL: https://atendimento-frontend-prod.herokuapp.com/
3. Commit nos demais branches: Build e Testes


## <a name="submit-pr"></a> Pull Request Submission

Before you submit your pull request consider the following guidelines:

* Make your changes in a new git branch:
```
  git checkout -b FIX_WRONGBUTTON_ATENDIMENTO-123
 ```
   Note that branch name is following the structure:
 ```
  <COMMIT_TYPE>_<SOHRT_DESCRIPTION>_<TICKET_ID> 
```

* Commit your changes using a descriptive commit message following the [commit types](#committypes) and the following structure:
```
  <type>(<scope>): <subject>: <tiket id in JIRA> 
```
```
  git commit -m “feat(navbar): Create an login button: ATENDIMENTO-223”
```

* Push your branch to GitHub:
```
  git push origin my-fix-branch
```

* In GitHub, configure a pull request to branch develop;
* Write a message body to explain “WHAT was done” and “WHY was done”. This helps the team to easily interpret your pull request and make comments if nedded. Do not forget to put the link of task in JIRA in the final of the body.
``` 
  Refactor the coupon UI

  Because:
  - The old UI code is fairly slow
  -There were a few unused dependencies
  -The old UI has aged poorly

  Solves: https://jiralabone.atlassian.net/browse/ATENDIMENTO-111
```
* Choose a person who you want to review your changes (at least one person);
* If you find that the Travis integration has failed, look into the logs on Travis to find out if your changes caused test failures;
* Before merge your branch, make sure if there are any comments;
* After your Pull Request is merged, delete your branch;

## <a name="committypes"></a> Commit types:

- **feat**: A new feature
- **fix**: A bug fix
- **docs**: Documentation only changes
- **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing
  semi-colons, etc)
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **perf**: A code change that improves performance
- **test**: Adding missing or correcting existing tests
- **chore**: Changes to the build process or auxiliary tools and libraries such as documentation
  generation
